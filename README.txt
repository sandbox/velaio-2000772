* ****************************************
* VELAIO theme, Simple installation      *
******************************************
1- Install Drupal 7
2- Install Omega Theme, Download from http://drupal.org/project/omega version 7.x-3.1
3- Install VELAIO theme. Enable the theme in admin/appearance �velaio 1.0� and �set default�. 


*****************************************
* VELAIO theme, Advanced installation   *
*****************************************
If you want to use the exactly VELAIO theme as demo http://demos.velaio.com/?theme=business you should follow these steps: 
1- Install Drupal 7.  Download from http://drupal.org/project/drupal version 7.22
2- Install Omega Theme, Download from http://drupal.org/project/omega version 7.x-3.1
3- Install VELAIO theme. 
4- Install the next modules:
	* adaptive_image = http://drupal.org/project/adaptive_image version = 7.x-1.4
	* ctools = http://drupal.org/project/ctools version = 7.x-1.3
	* libraries = http://drupal.org/project/libraries version = 7.x-2.1
	* views = http://drupal.org/project/views version = 7.x-3.7
	* views_slideshow = http://drupal.org/project/views_slideshow version = 7.x-3.0
5- Enable the next modules:	
	* Blog
	* PHP filter
	* Update manager
	* Chaos tools
	* Adaptive Image
	* Libraries
	* Views
	* Views Slideshow
	* Views Slideshow: Cycle
	* Views UI
6- Execute the file "demo_data_theme.sql" from "\sites\all\themes\velaio" in Drupal 7 database installation.  
7- Copy "files" from "\sites\all\themes\velaio" to "\sites\default" and replace all.
8- Copy "libraries" from "\sites\all\themes\velaio" to "\sites\all".
9- Enable the theme in admin/appearance "velaio 1.0" and "set default". 
