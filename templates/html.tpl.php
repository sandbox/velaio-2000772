<?php print $doctype; ?>
<?php $urli  = $_SERVER['REQUEST_URI']; ?>
<!--[if IE 7]><html class="ie7" lang="es" dir="ltr"><![endif]-->
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>  
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  
   <!--[if IE 7]>
   <link rel="stylesheet" type="text/css" href="http://galileo:8205/sites/all/themes/correvalle/css/iE7.css" />
<![endif]-->  
</head>
<body<?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
<!--<PLEASE DO NOT REMOVE OUR CREDITS, IF YOU WANT TO REMOVE VELAIO CREDITS CONTACT US AND PAY FOR OUR THEME-->
<div class="copyright">
<div id="credits">	Copyright &copy; 2013. All rights reserved. Dise&#241;o Web  &amp; Marketing en Internet by <a title="Marketing en Internet" href="http://www.velaio.com">VELAIO</a></div>
</div>
</body>
</html>